# ##### BEGIN GPL LICENSE BLOCK #####
#
#  Copyright (C) 2014-2017 script authors.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "Assetizer",
	"author": "DTRMediaWorks, BlenderAid",
	"version": (0, 1),
	"blender": (2, 79, 0),
	"location": "View3D > Add > Mesh > Assetizer",
	"description": "Object/Asset collection manager",
	"category": "DTR MediaWorks"
}

# Python libs.
import sys, subprocess, re, os, threading, struct, pprint, json, shutil

# Blender libs.
import bpy, bgl, blf
from bpy.types import AddonPreferences, Operator, Panel
from bpy.props import (
	BoolProperty,
	EnumProperty,
	FloatProperty,
	FloatVectorProperty,
	IntProperty,
	StringProperty,
	CollectionProperty
	)
from bpy_extras.io_utils import ExportHelper, ImportHelper

# FUTURE: How to handle default folder + no thumbnail icon with different themes?
# FUTURE: Add possibility to re-render thumbs or update theme using a different thumbnail scene.
# FUTURE: Support multiple asset dirs? Not really useful, could be done file system wise.
# FUTURE: Render append/link from single components.
# FUTURE: Performance optimization: Cache several cost intensive calls:
#         - Theme settings .. 
#         - Preferences
# FUTURE: Option to rename and/or center on safe.
# FUTURE: User adjustable GUI colors, use theme by default (maybe other theme colors then currently?)
# FUTURE: Better error handling? Were catch errors? User feedback will be helpful.

# Various functions to access paths inside the addon directory.
def assetizerHome(): return os.path.dirname(__file__)
def assetizerIcons(): return os.path.join(assetizerHome(), "icons")
def assetizerThumbnailer(): return os.path.join(assetizerHome(), "thumbnailer")

def log(s):
	"""
	Central log fn, lets modify logging behavior in a single place.
	"""
	print("[assetizer, %5i] - %s" % (threading.get_ident(), s))
	
def objPath(path): return os.path.splitext(path)[0] + ".obj"
def blendPath(path): return os.path.splitext(path)[0] + ".blend"     

# https://stackoverflow.com/questions/4417546/constantly-print-subprocess-output-while-process-is-running
def execute(cmd):
	"""
	Runs an external application, in this case the blender executable for
	thumbnail generation. Returns all lines written by this application to stdout
	immediately on a line by line basis.
	"""
	popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
	for stdout_line in iter(popen.stdout.readline, ""):
		yield stdout_line
	popen.stdout.close()
	_ = popen.wait()
	#if return_code:
	#    raise subprocess.CalledProcessError(return_code, cmd)

def createThumbnail(scene):
	"""
	Used to create the thumbnail for the object specified in parameter 'scene', full path
	is required. 
	"""
	log("*** CALL THUMBNAIL GEN ***")
	blenderExecutable = bpy.app.binary_path
	thumbscene = os.path.join(assetizerThumbnailer(), "Thumbnailer" + preferences().thumbnailScenePostfix() + ".blend")

	# Parse 'Tracing Sample' output to show as progress.
	ptrn = re.compile(".*Path Tracing Sample\\s+(\\d+)/\\d+.*")
	wm = bpy.context.window_manager
	wm.progress_begin(0, 300)
	
	# Run external instance of blender like the original thumbnail generator.
	cmd = [
		blenderExecutable, 
		"-b", thumbscene, 
		"--python-text", "ThumbScript", 
		"--", 
		"obj:" + scene, 
		"size:" + preferences().thumbnailRenderSize()
		]
	log(repr(cmd))
	
	for l in execute(cmd):
		# Log special prefixed lines (using log/log_object functions in thumbnail scripts).
		if l.startswith("[log]"):
			log(l.strip()[5:])

		# If contains progress analyze and update progress.
		pts = ptrn.match(l)
		if pts:
			wm.progress_update(int(pts.group(1)))
			sys.stdout.write("\r%f %%" % (float(pts.group(1))/3))
			sys.stdout.flush()

	print("")
	wm.progress_end()

def preferences():
	"""
	Return the instance of the AF user preferences.
	:rtype AssetizerPreferences
	"""
	return bpy.context.user_preferences.addons[__name__].preferences

def importObject(filepath, importType):
	"""
	Imports the specified object, prefer .blend if available, otherwise take .obj.
	"""
	# Deselect all, otherwise they were moved to the cursor position too.
	for obj in bpy.data.objects:
		obj.select = False
	
	# Cut off extension, import object by extension preference.
	basename, _ = os.path.splitext(filepath)
	log("Import object '%s'" % basename)

	# Prefer the blend to .obj.
	blend = basename + ".blend"
	if os.path.exists(blend):
		log("Use .blend version, link = %s" % repr(importType))
#         # https://blender.stackexchange.com/questions/34540/how-to-link-append-a-data-block-using-the-python-api
#         with bpy.data.libraries.load(basename + ".blend", link = link) as (dfrom, dto):
#             dto.objects = dfrom.objects
# 
#         for obj in dto.objects:
#             if obj is not None:
#                 log("  Append: %s" % obj)
#                 bpy.context.scene.objects.link(obj)

		# https://blender.stackexchange.com/questions/34299/appending-with-bpy-data-libraries-load-and-drivers

		files = []
		with bpy.data.libraries.load(blend) as (data_from, _):
			for name in data_from.groups:
				files.append({'name': name})

		global drawHandler
		bpy.types.SpaceView3D.draw_handler_remove(drawHandler, 'WINDOW')
		drawHandler = None

		if importType == "WORK":
			# bpy.ops.wm.read_homefile("INVOKE_DEFAULT", filepath=blend)
			# bpy.ops.wm.open_mainfile(filepath=blend)
			bpy.ops.work.assetizer("INVOKE_DEFAULT", filepath=blend)
		elif importType == "APPEND":
			bpy.ops.link.assetizer("INVOKE_DEFAULT", filepath=blend)
		elif importType == "CROWDS":
			bpy.ops.crowds.creator("INVOKE_DEFAULT", crowdAsset=os.path.basename(basename))

	# OK hopefully the obj exists.
	elif os.path.exists(basename + ".obj"):
		log("Use .obj version")
		bpy.ops.import_scene.obj(filepath=basename + ".obj")
		
	# In case we link, don't move to cursor.
	# if not link:        
	#     bpy.ops.view3d.snap_selected_to_cursor()
	# bpy.context.scene.objects.active = bpy.context.selected_objects[0]
		

class RenderTools:
	"""
	Handle drawing functionality in one place. If future versions of blender
	have a different API, changes are restricted to this class.
	"""
	@staticmethod
	def renderTexture(texture, x, y, width, height):
		bgl.glEnable(bgl.GL_BLEND)
		#bgl.glBlendFunc(bgl.GL_SRC_ALPHA, bgl.GL_ONE_MINUS_SRC_ALPHA)

		texture.gl_load()
		bgl.glBindTexture(bgl.GL_TEXTURE_2D, texture.bindcode[0])

		bgl.glEnable(bgl.GL_TEXTURE_2D)
		bgl.glTexParameteri(bgl.GL_TEXTURE_2D, bgl.GL_TEXTURE_MIN_FILTER, bgl.GL_NEAREST)
		bgl.glTexParameteri(bgl.GL_TEXTURE_2D, bgl.GL_TEXTURE_MAG_FILTER, bgl.GL_NEAREST)

		bgl.glColor4f(1, 1, 1, 1)

		bgl.glBegin(bgl.GL_QUADS)
		bgl.glTexCoord2d(0, 0)
		bgl.glVertex2d(x, y)
		bgl.glTexCoord2d(0, 1)
		bgl.glVertex2d(x, y + height)
		bgl.glTexCoord2d(1, 1)
		bgl.glVertex2d(x + width, y + height)
		bgl.glTexCoord2d(1, 0)
		bgl.glVertex2d(x + width , y)
		bgl.glEnd()

		texture.gl_free()

	@staticmethod
	def renderRect(color, x, y, width, height):
		bgl.glEnable(bgl.GL_BLEND)
		bgl.glColor4f(color[0], color[1], color[2], color[3])
		bgl.glRectf(x, y, x + width, y + height)

	@staticmethod
	def renderText(color, x, y, size, text, dpi = 72):
		bgl.glEnable(bgl.GL_BLEND)
		bgl.glColor4f(color[0], color[1], color[2], color[3])

		font_id = 0
		blf.position(font_id, x, y - size / 2, 0)
		blf.size(font_id, size, dpi)
		blf.draw(font_id, text)

class AssetizerPreferences(AddonPreferences):
	"""
	Preferences from addons menu. In addition, contains all information used
	for visualization. So it could easily be enhanced for visual settings.
	"""
	bl_idname = __name__

	datacenter_path = StringProperty(
		name="Data Center Path",
		subtype='FILE_PATH',
		default="E:\\DataCenter\\CircleOfLife\\"
		)

	project_path = StringProperty(
		name="Project Path",
		subtype='FILE_PATH',
		default="E:\\Shows\\CircleOfLife\\"
		)
	
	render_scene = EnumProperty(
		name="Thumbnail scene",
		items=[ 
			( "Original", "Original thumbnail scene", "Settings from previous version"),
			( "Gray", "Gray thumbnail scene", "Pure gray style scene"),
			( "Silver", "Silver thumbnail scene", "Asset is rendered in silver metal look"), 
		],
		default="Gray"
		)
	
	thumbnail_render_size = EnumProperty(
		name="Thumbnail render size",
		items=[
			( "64", "64x64", "Very small" ),
			( "128", "128x128", "Optimal size" ),
			( "256", "256x256", "Slow to render, much details"),
		],
		default="128"
		)

	def draw(self, context):
		layout = self.layout
		
		layout.row().prop(self, "project_path")
		layout.row().prop(self, "datacenter_path")
		layout.row().prop(self, "render_scene")
		layout.row().prop(self, "thumbnail_render_size")

	def thumbnailScenePostfix(self): return self.render_scene
	def thumbnailRenderSize(self): return self.thumbnail_render_size
		
	# http://blenderscripting.blogspot.de/2012/09/color-changes-in-ui.html
	def currentTheme(self):
		themeName = bpy.context.user_preferences.themes.items()[0][0]
		return bpy.context.user_preferences.themes[themeName]
	
	def currentThemeRadioButton(self):
		return self.currentTheme().user_interface.wcol_radio
	
	def iconSize(self): return 128
	def underlayWidth(self): return 180
	def menuItemMargins(self): return 4
	def menuItemHeight(self): return self.iconSize() + 2 * self.menuItemMargins()
	def menuItemWidth(self): return 400
	def itemTextSize(self): return 20
	def toolTipTextSize(self): return 10

	def bgColor(self): return (0, 0, 0, 0.6)
	
#     def menuColor(self): return (0.447, 0.447, 0.447, 0.8)
#     def menuColorSelected(self): return (0.555, 0.555, 0.555, 0.8)
#     def itemTextColor(self): return (0, 0, 0, 0.8)
	
	def menuColor(self): return (*self.currentThemeRadioButton().inner, 1)
	def menuColorSelected(self): return (*self.currentThemeRadioButton().inner_sel, 1)
	def itemTextColor(self): return (*self.currentThemeRadioButton().text, 1)
	def itemTextColorSelected(self): return (*self.currentThemeRadioButton().text_sel, 1)

class MenuItem:
	"""
	Represents an existing asset or folder. Handles drawing of the visual representation
	as well as every user interaction with it.
	"""
	def __init__(self, texture, deco, text, folderInfo = None, assetInfo = None):
		self._texture = texture
		self._deco = deco
		self._text = text
		self._folderInfo = folderInfo
		self._assetInfo = assetInfo

	def work(self, x, y):
		return y > 95 and y < 120

	def append(self, x, y):
		"""
		Check relative mouse position for link area (false for obj). 
		self._assetInfo[1] : blendExists
		self._assetInfo[3] : isShot
		"""
		return self._assetInfo[1] and y > 70 and y < 95 and not self._assetInfo[3] 

	def crowds(self, x, y):
		"""
		Check relative mouse position for appending area.
		"""
		return self._assetInfo[1] and y > 45 and y < 70 and not self._assetInfo[3] 



	def draw(self, rect, mouse):
		"""
		Draws the menu item in the given rect.
		"""
		x, y, w, h = rect
		mx, my = mouse[0] - x, mouse[1] - y
		isInside = mx >= 0 and mx < w and my >= 0 and my < h

		p = preferences()
		margins = p.menuItemMargins()
		iconSize = p.iconSize()
		
		texts = p.itemTextSize()
		ttexts = p.toolTipTextSize()
		textc = p.itemTextColor()
		textcs = p.itemTextColorSelected()
		textx = x + margins + p.underlayWidth()

		# Render background rectangle.
		RenderTools.renderRect(
			p.menuColorSelected() if isInside else p.menuColor(),
			x + margins,
			y + margins,
			w - 2 * margins,
			h - 2 * margins
		)

		# Render a type specific background.
		RenderTools.renderTexture(
			self._deco,
			x + 2 * margins,
			y + margins,
			p.underlayWidth(),
			iconSize
		)
		# Incase of non uniform resolution, We calculate custom width & height of icon
		aspect = self._texture.size[0] / self._texture.size[1]
		# Icon Margin. 
		icM = 12
		# Apply icon margin only if  if aspect ratio is not 1
		icW = iconSize - (icM - int(icM / aspect))
		icH = icW / aspect
		# Render icon.
		RenderTools.renderTexture(
			self._texture,
			x + 2 * margins + (iconSize - icW)/2,
			y + margins + (iconSize - icH)/2,
			icW,
			icH
		)

		# Render text for asset.
		RenderTools.renderText(
			textcs if isInside else textc, 
			textx, 
			y + 26, 
			texts, 
			self._text
		)

		if self._assetInfo:
			_, blendExists, renderer, isShot = self._assetInfo
			append, crowds, work = False, False, False
			
			if isInside:
				if self.work(mx, my):
					work = True
					renderer.setInfo("Open copy of '%s' to work on it ..." % self._text)

				if self.append(mx, my):
					append = True
					renderer.setInfo("Append/Link '%s' to current file ..." % self._text)

				if self.crowds(mx, my):
					crowds = True
					renderer.setInfo("Add crowd of '%s' to shot ..." % self._text)
			
			if work:
				RenderTools.renderText((*textcs[0:3], 1), textx, y + 107, ttexts, "Work on this {0}".format("shot" if isShot else "asset"))
			else:
				RenderTools.renderText((*textcs[0:3], 0.3), textx, y + 107, ttexts, "Work on this {0}".format("shot" if isShot else "asset"))

			if blendExists and not isShot:
				if append and not isShot:
					RenderTools.renderText((*textcs[0:3], 1), textx, y + 82, ttexts, "Click to Append/Link")
				else:
					RenderTools.renderText((*textcs[0:3], 0.3), textx, y + 82, ttexts, "Click to Append/Link")
				
				if crowds:
					RenderTools.renderText((*textcs[0:3], 1), textx, y + 57, ttexts, "Click to add as Crowd")
				else:
					RenderTools.renderText((*textcs[0:3], 0.3), textx, y + 57, ttexts, "Click to add as Crowd")

					

	def testClick(self, rect, mouse):
		"""
		Handle user click, can be everywhere!
		"""
		x, y, w, h = rect
		mx, my = mouse[0] - x, mouse[1] - y
		isInside = mx >= 0 and mx < w and my >= 0 and my < h

		# If clicked inside this instance ...
		if isInside:
			# Check if its a folder entry ..
			if self._folderInfo:
				# Update menu in renderer.
				projectPath, sequenceName, renderer = self._folderInfo
				# renderer.setMenuItems(MenuItem.buildListForFolder(*self._folderInfo))
				if sequenceName == "..":
					renderer.setMenuItems(MenuItem.buildListOfSequences(projectPath, renderer))
				else:
					renderer.setMenuItems(MenuItem.buildListOfShots(projectPath ,sequenceName, renderer))
			elif self._assetInfo:
				# If this is an asset, import, link or whatever.
				full, _, renderer, _ = self._assetInfo
				# Decide whether append or link (if supported).
				if self.append(mx, my):
					importObject(full, "APPEND")
					renderer.setFinished()

				if self.crowds(mx, my):
					importObject(full, "CROWDS")
					renderer.setFinished()

				if self.work(mx, my):
					importObject(full, "WORK")
					renderer.setFinished()

	@staticmethod
	def buildListForFolder(path, level, renderer):
		"""
		Create the list of menu items for the given path. Level
		is used to determine if the top level has been reached (.. prevented).
		"""
		r = []
		if level != 0:
			r.append(MenuItem(
				renderer.folderIcon(),
				renderer.decoDirIcon(),
				"..",
				folderInfo = (os.path.abspath(os.path.join(path, os.pardir)), level - 1, renderer)
			))

		# Folders first ...
		for e in sorted(os.listdir(path)):
			full = os.path.join(path, e)
			if os.path.isdir(full):
				r.append(MenuItem(
					renderer.folderIcon(),
					renderer.decoDirIcon(),
					e,
					folderInfo = (full, level + 1, renderer)
				))

		for e in sorted(os.listdir(path)):
			full = os.path.join(path, e)
			basename, ext = os.path.splitext(full)
			if ext.lower() == ".obj":
				iconImage = basename + ".png"
				icon = renderer.loadIcon(iconImage, False) if os.path.exists(iconImage) else renderer.noThumbnailIcon()
				blendExists = os.path.exists(basename + ".blend")
				deco = renderer.decoBlendIcon() if blendExists else renderer.decoObjIcon()
				if not os.path.isdir(full):
					r.append(MenuItem(
						icon,
						deco,
						os.path.basename(basename),
						assetInfo = (full, blendExists, renderer, False) #Final parameter is 'isShot'
					))

		return r

	@staticmethod
	def buildListOfAssets(path, renderer):
		assetList = []
		ret = []
		try:
			with open(path + "showConfig.json") as dataFile:
				assetList = json.load(dataFile)["assets"]
		except Exception as e:
			raise e
		for eachAssetName in assetList:
			basePath = path + "assets\\{0}\\{0}".format(eachAssetName)
			if os.path.isfile(blendPath(basePath)):
				iconImage = basePath + ".png"
				icon = renderer.loadIcon(iconImage, False) if os.path.exists(iconImage) else renderer.noThumbnailIcon()
				deco = renderer.decoBlendIcon()
				ret.append(MenuItem(
						icon,
						deco,
						eachAssetName,
						assetInfo = (basePath, True, renderer, False)
					))
		return ret

	@staticmethod
	def buildListOfSequences(path, renderer):
		sequencesList = []
		ret = []
		try:
			with open(path + "showConfig.json") as dataFile:
				sequencesList = json.load(dataFile)["sequences"]
		except Exception as e:
			raise e
		
		for eachSeq in sequencesList:
			seqName = eachSeq["sequenceOrder"] + "_" +eachSeq["sequenceName"]
			seqPath = path + "\\" + seqName
			if os.path.isdir(seqPath):
				ret.append(MenuItem(
						renderer.folderIcon(),
						renderer.decoDirIcon(),
						seqName,
						folderInfo = (path, seqName, renderer)
					))
		return ret

	@staticmethod
	def buildListOfShots(path, sequence, renderer):
		noOfShots = 0
		shotPrefix = ""
		ret = []
		try:
			with open(path + "showConfig.json") as dataFile:
				seqList = json.load(dataFile)["sequences"]
		except Exception as e:
			raise e

		for eachSeq in seqList :
			seqName = eachSeq["sequenceOrder"] + "_" +eachSeq["sequenceName"]
			if seqName == sequence:
				noOfShots = eachSeq["noOfShots"]
				shotPrefix = eachSeq["sequenceName"]

		ret.append(MenuItem(
				renderer.folderIcon(),
				renderer.decoDirIcon(),
				"..",
				folderInfo = (path, ".." , renderer)
			))

		for i in range(noOfShots):
			shotPath = path + sequence + "\\{0}_{1}\\{0}_{1}".format(shotPrefix, ("%02d" % (i,)))
			if os.path.isfile(blendPath(shotPath)):
				iconImage = shotPath + ".png"
				icon = renderer.loadIcon(iconImage, False) if os.path.exists(iconImage) else renderer.noThumbnailIcon()
				deco = renderer.decoBlendIcon()
				ret.append(MenuItem(
						icon,
						deco,
						"{0}_{1}".format(shotPrefix, ("%02d" % (i,))),
						assetInfo = (shotPath, True, renderer, True) #Final parameter is 'isShot'
					))
		return ret


class ScreenRenderer:
	"""
	Handles the rendering of the asset selection OSD as well as user interaction
	with the OSD. Manages loaded textures and cleans them up when calling dispose() which
	is done before releasing the object.
	"""
	def __init__(self):
		self._dbg = ""
		self._nfo = ""
		self._finished = False

		# Last known mouse positions.
		self._mouseX = -1
		self._mouseY = -1

		# Store screen dimension in last draw call.
		self._width = -1
		self._height = -1

		# Scroll through assets.
		self._scrollPos = 0
		self._maxScroll = 0

		# Limit the asset menu area (for e.g. later additions).
		self._menuTop = 20

		self._items = []

		# Lists to store info about loaded textures (for later cleanup).
		self._genericIcons = []
		self._specificIcons = []

		# Some icons for multiple uses.
		self._folderIcon = self.loadIcon(os.path.join(assetizerIcons(), "folder.png"), True)
		self._noThumbnailIcon = self.loadIcon(os.path.join(assetizerIcons(), "nothumbnail.png"), True)
		self._decoObjIcon = self.loadIcon(os.path.join(assetizerIcons(), "button-decoration-obj.png"), True)
		self._decoBlendIcon = self.loadIcon(os.path.join(assetizerIcons(), "button-decoration-blend.png"), True)
		self._decoDirIcon = self.loadIcon(os.path.join(assetizerIcons(), "button-decoration-dir.png"), True)

	def loadIcon(self, path, generic):
		"""
		Load icon as texture and return id. Stores information for later cleanup in one
		of the internal lists. One will cleanup of on every folder change, the other on final cleanup.
		"""
		log("Load image: " + path)
		tid = bpy.data.images.load(filepath = path, check_existing = True)
		if generic:
			self._genericIcons.append(tid.filepath_raw)
		else:
			self._specificIcons.append(tid.filepath_raw)
		#log("   RAW: " + tid.filepath_raw)
		return tid

	def freeImages(self, lst):
		"""
		Free textures not needed anymore.
		"""
		for image in bpy.data.images:
			if image.filepath_raw in lst:
				#log("CLEAN TEX:" + image.filepath_raw)
				image.user_clear()
				bpy.data.images.remove(image, do_unlink = True)
		lst.clear()

	def dispose(self):
		"""
		Called on termination, frees all loaded resources.
		"""
		self.freeImages(self._genericIcons)
		self.freeImages(self._specificIcons)

	def folderIcon(self): return self._folderIcon
	def noThumbnailIcon(self): return self._noThumbnailIcon
	def decoObjIcon(self): return self._decoObjIcon
	def decoBlendIcon(self): return self._decoBlendIcon
	def decoDirIcon(self): return self._decoDirIcon

	def setMenuItems(self, items):
		"""
		Set new list of menu items.
		"""
		# Clear previous resources.
		#self.freeImages(self._specificIcons)
		self._items = items

	def setFinished(self):
		"""
		Set finish state from outside (used after asset load).
		"""
		self._finished = True

	def isFinished(self):
		"""
		Reports if finished is reached (if asset has been load or any user interaction).
		"""
		return self._finished
	
	def setInfo(self, s):
		self._nfo = s

	def renderInfo(self, height):
		"""
		Render textual info in the top area.
		"""
		if len(self._nfo) > 0:
			RenderTools.renderText((0.8, 0.8, 0.8, 1), 5, height - 8, 16, self._nfo)

	def renderDebug(self, width):
		"""
		Render text in the lower area.
		"""
		if len(self._dbg) > 0:
			RenderTools.renderRect(preferences().bgColor(), 0, 0, width, self._menuTop)
			RenderTools.renderText((0.6, 0.6, 1, 1), 5, 8, 16, self._dbg)

	def scrollArea(self, _, width, height):
		"""
		Total height of all menu items. Used to determine scroll area.
		"""
		count = len(self._items)
		itemsPerLine = round(width / preferences().menuItemWidth())
		lines = round(count / itemsPerLine) + (1 if (count % itemsPerLine) != 0 else 0)
		maxHeight = lines * preferences().menuItemHeight()
		displayHeight = height - self._menuTop

		if displayHeight >= maxHeight:
			return 0
		return maxHeight - displayHeight

	def calcMenuItemRect(self, index, _, width, height):
		"""
		Based on valid area size, return the position of item number 'index', if count items should be rendered.
		"""
		itemsPerLine = round(width / preferences().menuItemWidth())
		effectiveItemWidth = width / itemsPerLine

		col = index % itemsPerLine
		row = int(index / itemsPerLine)

		# self._dbg = "w: %f, c: %f, ipl: %f, lns: %f, eiw: %f" % (width, count, itemsPerLine, lines, effectiveItemWidth)
		# log("%i - %f, %f" % (index, row, col))
		# print((
		#     col * effectiveItemWidth,
		#     height - row * preferences().menuItemHeight(),
		#     effectiveItemWidth,
		#     preferences().menuItemHeight()
		# ))

		return (
			col * effectiveItemWidth,
			height - (row + 1) * preferences().menuItemHeight() - self._menuTop + self._scrollPos,
			effectiveItemWidth,
			preferences().menuItemHeight()
		)

	def draw(self, width, height):
		self._nfo = "Press ESC or RMB to cancel ..."
		self._width = width
		self._height = height

		# Render the background darker.
		RenderTools.renderRect(preferences().bgColor(), 0, 0, width, height)

		# Update scroll info.
		self._maxScroll = self.scrollArea(len(self._items), width, height)
		if self._scrollPos > self._maxScroll:
			self._scrollPos = self._maxScroll
		#self._dbg = "%f" % self._maxScroll

		# Limit drawing area.
		#bgl.glScissor(0, 0, width, height - self._menuTop)
		#bgl.glEnable(bgl.GL_SCISSOR_TEST)

		# Render the menu items.
		if not self._items:
			# Show informational text at the top.
			self._nfo = "No items in asset folder, verify path (%s)." % preferences().datacenter_path
		else:
			# Render item by item.
			for index, e in enumerate(self._items):
				# The target rectangle, based on current view size.
				itemRect = self.calcMenuItemRect(index, len(self._items), width, height)
				e.draw(itemRect, (self._mouseX, self._mouseY))

		#bgl.glDisable(bgl.GL_SCISSOR_TEST)

		# Render another dark rectangle on top (info area).
		RenderTools.renderRect(preferences().bgColor(), 0, height - self._menuTop, width, self._menuTop)
		self.renderInfo(height)
		
		self.renderDebug(width)

		# Cleanup render states.
		bgl.glDisable(bgl.GL_BLEND)
		bgl.glDisable(bgl.GL_TEXTURE_2D)

	def mouseMove(self, x, y):
		self._mouseX = x
		self._mouseY = y

	def mouseClick(self, x, y, button, event):
		#self._dbg = "%f, %f, %s, %s" % (x, y, button, event)
		# Prevent crash.
		if self._width < 1 or self._height < 1:
			return

		if button == "LEFTMOUSE" and event == "RELEASE":
			for index, e in enumerate(self._items):
				# The target rectangle, based on current view size.
				itemRect = self.calcMenuItemRect(index, len(self._items), self._width, self._height)
				e.testClick(itemRect, (x, y))

		if button == "RIGHTMOUSE":
			self._finished = True

	def wheel(self, up):
		"""
		Handle mouse wheel event.
		"""
		scrollAmount = 72
		if not up:
			self._scrollPos += scrollAmount
			if self._scrollPos > self._maxScroll:
				self._scrollPos = self._maxScroll
		else:
			self._scrollPos = 0 if self._scrollPos < scrollAmount else self._scrollPos - scrollAmount

	def otherEvent(self, event):
		if event.type == "ESC":
			self._finished = True


drawHandler = None

class AssetizerMenu(Operator):
	"""
	Renders the OSD menu to select an existing asset for import.
	"""
	bl_idname = "view3d.assetizer"
	bl_label = "Assets Menu"
	bl_options = {'REGISTER', 'UNDO'}

	def __init__(self):
		self._renderer = None
		# self._handle = None

	def modal(self, context, event):
		context.area.tag_redraw()

		# Forward several events to the renderer.
		if event.type == 'MOUSEMOVE':
			self._renderer.mouseMove(event.mouse_region_x, event.mouse_region_y)
		elif event.type == "LEFTMOUSE" or event.type == "MIDDLEMOUSE" or event.type == "RIGHTMOUSE":
			self._renderer.mouseClick(event.mouse_region_x, event.mouse_region_y, event.type, event.value)
		elif event.type == "WHEELUPMOUSE" or event.type == "WHEELDOWNMOUSE":
			self._renderer.wheel(event.type == "WHEELUPMOUSE")
		else:
			#log(repr(event.type))
			self._renderer.otherEvent(event)

		# Check if renderer signals that it can be removed.
		if self._renderer.isFinished():
			global drawHandler
			if drawHandler:
				bpy.types.SpaceView3D.draw_handler_remove(drawHandler, 'WINDOW')
			self._renderer.dispose()
			self._renderer = None
			return {'FINISHED'}

		return {'RUNNING_MODAL'}

	def invoke(self, context, event):
		"""
		Called if user triggers the operator. Adds a custom render callback.
		"""
		# Only this area type is supported.
		if context.area.type == 'VIEW_3D':
			self._renderer = ScreenRenderer()
			self._renderer.setMenuItems(MenuItem.buildListOfAssets(preferences().project_path, self._renderer))

			global drawHandler
			drawHandler = bpy.types.SpaceView3D.draw_handler_add(self.drawCallback, (context, ), 'WINDOW', 'POST_PIXEL')
			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "View3D not found, cannot show assets menu")
			return {'CANCELLED'}

	def drawCallback(self, context):
		self._renderer.draw(
			context.area.regions[4].width,
			context.area.regions[4].height
		)

class ShotsMenu(Operator):
	bl_idname = "view3d.shotsmenu"
	bl_label = "Shots Menu"
	bl_options = {'REGISTER', 'UNDO'}

	def __init__(self):
		self._renderer = None
		# self._handle = None

	def modal(self, context, event):
		context.area.tag_redraw()

		# Forward several events to the renderer.
		if event.type == 'MOUSEMOVE':
			self._renderer.mouseMove(event.mouse_region_x, event.mouse_region_y)
		elif event.type == "LEFTMOUSE" or event.type == "MIDDLEMOUSE" or event.type == "RIGHTMOUSE":
			self._renderer.mouseClick(event.mouse_region_x, event.mouse_region_y, event.type, event.value)
		elif event.type == "WHEELUPMOUSE" or event.type == "WHEELDOWNMOUSE":
			self._renderer.wheel(event.type == "WHEELUPMOUSE")
		else:
			#log(repr(event.type))
			self._renderer.otherEvent(event)

		# Check if renderer signals that it can be removed.
		if self._renderer.isFinished():
			# bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
			global drawHandler
			if drawHandler:
				bpy.types.SpaceView3D.draw_handler_remove(drawHandler, 'WINDOW')
			self._renderer.dispose()
			self._renderer = None
			return {'FINISHED'}

		return {'RUNNING_MODAL'}

	def invoke(self, context, event):
		"""
		Called if user triggers the operator. Adds a custom render callback.
		"""
		# Only this area type is supported.
		if context.area.type == 'VIEW_3D':
			self._renderer = ScreenRenderer()
			self._renderer.setMenuItems(MenuItem.buildListOfSequences(preferences().project_path, self._renderer))
			# self._handle = bpy.types.SpaceView3D.draw_handler_add(self.drawCallback, (context, ), 'WINDOW', 'POST_PIXEL')
			global drawHandler
			drawHandler = bpy.types.SpaceView3D.draw_handler_add(self.drawCallback, (context, ), 'WINDOW', 'POST_PIXEL')
			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "View3D not found, cannot show shots menu")
			return {'CANCELLED'}

	def drawCallback(self, context):
		self._renderer.draw(
			context.area.regions[4].width,
			context.area.regions[4].height
		)


'''
class AssetizerExport(Operator, ExportHelper):
	"""
	Manages all export related tasks. Uses ExportHelper for export file selection.
	"""
	bl_idname = "export.assetizer"
	bl_label = "Assetizer | Export"
	bl_options = {'REGISTER'}

	# Exporter stuff.
	filename_ext = ".blend"
	filter_glob = StringProperty(default="*.blend", options={'HIDDEN'})

	def execute(self, context):
		"""
		This gets called after user has selected a file for export.
		"""
		###########################################
		# .blend export

		# https://docs.blender.org/api/blender_python_api_2_77_1/bpy.types.BlendDataLibraries.html
		log("Export as .blend")
		bpy.data.libraries.write(
			blendPath(self.properties.filepath), 
			set(bpy.context.selected_objects), 
			relative_remap = True
		)

		###########################################
		# .obj export and thumb generation

		# Write wavefront obj to file.
		log("Export as .obj");
		bpy.ops.export_scene.obj(
			filepath = objPath(self.properties.filepath),
			use_selection = True,
			use_mesh_modifiers = True,
			use_materials = False
		)

		# Run thumbnail generator for this .obj.
		log("Create thumbnail.")
		createThumbnail(
			objPath(self.properties.filepath)
		)

		###########################################
		# Done
		log("Completed.")
		return {'FINISHED'}
'''
class Assets(bpy.types.PropertyGroup):
	name = StringProperty(name="Asset Name")

class Assetize(Operator):
	"""docstring for Assetize"""
	bl_idname = "export.assetizer"
	bl_label = "Assetize Asset"
	bl_options = {'REGISTER'}

	assets = CollectionProperty(type = Assets)
	assetToExport = StringProperty(name="Export as")
	versionComments = StringProperty(name="Assetization Comments")

	def __init__(self):
		super(Assetize, self).__init__()
		self._showData = None
		self._assetData = None

	def invoke(self, context, event):
		try:
			with open(preferences().project_path + "showConfig.json") as dataFile:
				self._showData = json.load(dataFile)
			self.assets.clear()
			assetNames = self._showData["assets"]
			for assetName in assetNames:
				asset = self.assets.add()
				#This will auto fill assetExport depending on name of scene
				if assetName == context.scene.asset_info.assetName:
					self.assetToExport = assetName
				asset.name = assetName
		except Exception as e:
			self._showData = None
			self.report ({"ERROR"},"Please check project preferences")

		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		if self._showData == None:
			layout.label("Project Preferences are not valid. Please fix it first!!")
			return
		layout.prop_search(self, "assetToExport", self, "assets")
		selectedIndex = self.assets.find(self.assetToExport)
		if selectedIndex != -1:
			try:
				with open(preferences().project_path + "assets\\" + self.assetToExport + "\\info.json") as dataFile:
					self._assetData = json.load(dataFile)
				layout.label( "Current version of {0} is {1}".format(self.assetToExport ,self._assetData["version"]) )
				layout.prop(self, "versionComments")
			except Exception as e:
				layout.label( "Error Occured while reading Version Info") 

	def execute(self, context):
		if self._showData == None:
			self.report ({"ERROR"},"Please check project preferences")
			return {"CANCELLED"}
		if len(self.assetToExport) == 0 or self._assetData["version"] == None:
			self.report ({"ERROR"},"Please Select which asset to export")
			return {"CANCELLED"}

		#Update assetData
		self._assetData["version"] = self._assetData["version"] + 1
		self._assetData["versionComments"].append({
				"version" : self._assetData["version"],
				"comments" : self.versionComments
			})
		# context.scene.name = self.assetToExport
		context.scene.asset_info.assetName = self._assetData["name"]
		context.scene.asset_info.assetType = self._assetData["type"]
		context.scene.asset_info.assetVersion = self._assetData["version"]
		
		bpy.ops.wm.save_as_mainfile(
				filepath = preferences().project_path + "assets\\{0}\\{0}.blend".format(self.assetToExport),
				check_existing = False,
				relative_remap = False
			)

		with open(preferences().project_path + "assets\\" + self.assetToExport + "\\info.json", "w") as dataFile:
			json.dump(self._assetData, dataFile, indent=2)

		os.makedirs(preferences().datacenter_path + "assets\\" + self.assetToExport, exist_ok=True)

		#Version 
		bpy.ops.wm.save_as_mainfile(
				filepath = preferences().datacenter_path + "assets\\{0}\\{0}_v{1}.blend".format(self.assetToExport, self._assetData["version"]),
				check_existing = True,
				copy = True,
				relative_remap = False
			)
		###########################################
		# .obj export and thumb generation

		# Write wavefront obj to file.
		log("Export as .obj");
		objFilePath = preferences().project_path + "assets\\{0}\\{0}.obj".format(self.assetToExport)
		bpy.ops.export_scene.obj(
			filepath = objFilePath,
			use_selection = True,
			use_mesh_modifiers = True,
			use_materials = False
		)

		# Run thumbnail generator for this .obj.
		log("Create thumbnail.")
		createThumbnail(
			objFilePath
		)
		log("Completed.")
		###########################################
		self.report ({"INFO"},"{} is exported Successfully".format(self.assetToExport))
		return {"FINISHED"}

	def check(self, context):
		return True


class AssetGroups(bpy.types.PropertyGroup):
	selected = BoolProperty(default=True)
	name = StringProperty()


class AssetLinker(Operator):
	bl_idname = "link.assetizer"
	bl_label = "Import Asset"
	bl_options = {'REGISTER'}

	filepath = StringProperty(default="")
	link = BoolProperty(default=True, name="Do you want to Link instead of appending")
	groups = CollectionProperty(type=AssetGroups)

	def invoke(self, context, event):
		with bpy.data.libraries.load(self.filepath) as (data_from, _):
			self.groups.clear()
			self.names = []
			for name in data_from.groups:
				grp = self.groups.add()
				grp.name = name

		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		if len(self.groups) == 0:
			layout.label("No Groups found in selected asset.")
			return
		layout.prop(self, "link");
		layout.label("----Select Groups---")
		for grp in self.groups:
			layout.prop(grp, "selected", text = grp.name)

	def execute(self, context):
		if len(self.groups) == 0:
			self.report ({"ERROR"},"No Groups found in selected asset.")
			return {"CANCELLED"}

		files = []
		# with bpy.data.libraries.load(self.filepath) as (data_from, _):
		#     for name in data_from.groups:
		#         files.append({'name': name})
		for grp in self.groups:
			if grp.selected:
				files.append({"name" : grp.name})
		noOfGroups = len(files)
		if noOfGroups == 0:
			self.report ({"ERROR"},"No group is selected for importing")
			return {"CANCELLED"}

		if self.link:
			bpy.ops.wm.link(directory=self.filepath + "/Group/", files=files)
		else:
			bpy.ops.wm.append(directory=self.filepath + "/Group/", files=files)

		self.report ({"INFO"},"Successfully {0} {1} group(s)".format("linked" if self.link else "appended", noOfGroups))

		return {"FINISHED"}



class Sequences(bpy.types.PropertyGroup):
	name = StringProperty(name="Sequence Name")


class Shots(bpy.types.PropertyGroup):
	name = StringProperty(name="Shot Name")


class CrowdsCreator(Operator):
	bl_idname = "crowds.creator"
	bl_label = "Generate Crowds"
	bl_options = {'REGISTER'}

	def __init__(self):
		super(CrowdsCreator, self).__init__()
		self._showData = None
		self._shotData = None

	sequences = CollectionProperty(type=Sequences)
	shots = CollectionProperty(type=Shots)
	assets = CollectionProperty(type = Assets)
	selectedSequence = StringProperty(name="Sequence")
	selectedShot = StringProperty(name="Shot")
	crowdAsset = StringProperty(name="Crowd Asset")
	noOfCrowds = IntProperty(name="Crowd Count")
	groups = CollectionProperty(type = AssetGroups)
	selectedGroup = StringProperty("Selected Group")
	proxyObjectName = StringProperty(name="Rig Object", default= "Armature")

	def invoke(self, context, event):
		try:
			with open(preferences().project_path + "showConfig.json") as dataFile:
				self._showData = json.load(dataFile)
			self.selectedSequence = context.scene.asset_info.sequenceName
			self.selectedShot = context.scene.asset_info.assetName
			self.sequences.clear()
			seqData = self._showData["sequences"]
			for eachSeqData in seqData:
				seq = self.sequences.add()
				seq.name = eachSeqData["sequenceOrder"] + "_" + eachSeqData["sequenceName"]

				# Helps to auto fill from scene name
				if seq.name == context.scene.name.split("/")[0]:
					self.selectedSequence = seq.name

			self.assets.clear()
			assetNames = self._showData["assets"]
			for assetName in assetNames:
				asset = self.assets.add()
				asset.name = assetName
		except Exception as e:
			self._showData = None
			self.report ({"ERROR"},"Please check project preferences {0}".format(str(e)))
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		if self._showData == None:
			layout.label("Project Preferences are not valid. Please fix it first!!")
			return

		layout.prop_search(self, "crowdAsset", self, "assets")
		selectedAssetIndex = self.assets.find(self.crowdAsset)
		if selectedAssetIndex == -1:
			return

		crowdAssetFilePath = preferences().project_path + "assets\\{0}\\{0}.blend".format(self.crowdAsset)
		with bpy.data.libraries.load(crowdAssetFilePath) as (data_from, _):
			self.groups.clear()
			for name in data_from.groups:
				grp = self.groups.add()
				grp.name = name
		if len(self.groups) == 0:
			layout.label("There are no groups in selected asset")
			return
		layout.prop_search(self, "selectedGroup", self, "groups")
		selectedGroupIndex = self.assets.find(self.crowdAsset)
		if selectedGroupIndex == -1:
			return
		layout.prop(self, "proxyObjectName")

		
		layout.prop_search(self, "selectedSequence", self, "sequences")
		selectedSeqIndex = self.sequences.find(self.selectedSequence)
		if selectedSeqIndex == -1:
			return
		seqData = None
		for eachSeqData in self._showData["sequences"]:
			if eachSeqData["sequenceOrder"] + "_" + eachSeqData["sequenceName"] == self.selectedSequence:
				seqData = eachSeqData

		self.shots.clear()
		for i in range(seqData["noOfShots"]):
			shot = self.shots.add()
			shot.name = seqData["sequenceName"] + "_" + ("%02d" % (i,))

		layout.prop_search(self, "selectedShot", self, "shots")
		selectedShotIndex = self.shots.find(self.selectedShot)
		if selectedShotIndex == -1:
			return
		layout.prop(self, "noOfCrowds")

		try:
			with open(preferences().project_path + self.selectedSequence + "\\" + self.selectedShot + "\\info.json") as dataFile:
				self._shotData = json.load(dataFile)
		except Exception as e:
			self._shotData = None
			layout.label( "Error Occured while reading crowd Info")
			return

		allCrowdData = self._shotData.get("crowds")
		if not allCrowdData:
			allCrowdData = {}

		allCrowdData[self.crowdAsset] = self.noOfCrowds
		self._shotData["crowds"] = allCrowdData

	def execute(self, context):
		if not self.selectedSequence or not self.selectedShot or not self.crowdAsset or not self.selectedGroup:
			self.report ({"ERROR"},"Asset, Group, Sequence and Shot aren't properly selected")
			return {"CANCELLED"}

		if self._shotData == None:
			self.report ({"ERROR"},"There's something wrong while reading shotData")
			return {"CANCELLED"}
		
		assetFilePath = preferences().project_path + "assets\\{0}\\{0}.blend".format(self.crowdAsset)
		if not os.path.exists(assetFilePath):
			self.report ({"ERROR"},"Asset file doesn't exists")
			return {"CANCELLED"}
		crowdFolderPath = preferences().project_path + self.selectedSequence + "\\" + self.selectedShot + "\\crowds\\" + self.crowdAsset 
		if os.path.exists(crowdFolderPath):
			shutil.rmtree(crowdFolderPath)
		os.makedirs(crowdFolderPath, exist_ok=True)

		#Symlink or copy textures folder too
		assetTexturesFolder = os.path.dirname(assetFilePath) + "\\textures"
		try:
			assetTexturesRelativePath = os.path.relpath(assetTexturesFolder, crowdFolderPath)
			os.symlink(assetTexturesRelativePath, crowdFolderPath + "\\textures")
		except Exception as e:
			print ("Unable symlink textures {0}".format(e))
			shutil.copytree(assetTexturesFolder, crowdFolderPath + "\\textures")

		crowdFilePath = ""
		symLinkException = False
		for i in range(self.noOfCrowds):
			crowdFilePath = "{0}\\{1}_{2}.blend".format(crowdFolderPath, self.crowdAsset, i+1)
			try:
				assetRelativePath = os.path.relpath(assetFilePath, crowdFolderPath)
				os.symlink(assetRelativePath, crowdFilePath)
			except OSError as e:
				shutil.copyfile(assetFilePath, crowdFilePath)
				symLinkException = True
			bpy.ops.wm.link(directory=crowdFilePath + "/Group/", files=[{"name" : self.selectedGroup}])
			grpObject = bpy.context.scene.objects.active
			grpObject.hide_select = True
			bpy.ops.object.proxy_make(object="Armature")
			log(bpy.context.scene.objects.active)

		with open(preferences().project_path + self.selectedSequence + "\\" + self.selectedShot + "\\info.json", "w") as dataFile:
			json.dump(self._shotData, dataFile, indent=2)
		if (symLinkException):
				self.report({'WARNING'}, "Files copied instead of Symbolid Linking as Blender is not running in admin mode")

		return {"FINISHED"}

	def check(self, context):
		return True


class CrowdUpdater(Operator):
	bl_idname = "crowds.updater"
	bl_label = "Update Crowds"
	bl_options = {'REGISTER'}

	def __init__(self):
		super(CrowdUpdater, self).__init__()
		self._shotData = None

	def invoke(self, context, event):
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		if not context.scene.asset_info.assetType == "SHOT":
			layout.label("You're not in SHOT context")
			return
		try:
			with open(preferences().project_path + context.scene.asset_info.sequenceName + "\\" + context.scene.asset_info.assetName + "\\info.json") as dataFile:
				self._shotData = json.load(dataFile)
		except Exception as e:
			self._shotData = None
			layout.label("Error while reading SHOT data")
			return
		crowdData = self._shotData["crowds"]
		for key in crowdData.keys():
			layout.label("{} : {}".format(key, crowdData[key]))

	def execute(self, context):
		if self._shotData == None:
			self.report ({"ERROR"},"Shot data is not read properly")
			return {"CANCELLED"}
		crowdData = self._shotData["crowds"]
		symLinkException = False
		for key in crowdData.keys():
			crowdFolderPath = preferences().project_path + context.scene.asset_info.sequenceName + "\\" + context.scene.asset_info.assetName + "\\crowds\\" + key 
			if os.path.exists(crowdFolderPath):
				shutil.rmtree(crowdFolderPath)
			os.makedirs(crowdFolderPath, exist_ok=True)
			assetFilePath = preferences().project_path + "assets\\{0}\\{0}.blend".format(key)
			noOfCrowds = crowdData[key]
			for i in range(noOfCrowds):
				crowdFilePath = "{0}\\{1}_{2}.blend".format(crowdFolderPath, key, i+1)
				# shutil.copyfile(assetFilePath, crowdFilePath)
				try:
					assetRelativePath = os.path.relpath(assetFilePath, crowdFolderPath)
					os.symlink(assetRelativePath, crowdFilePath)
				except OSError as e:
					shutil.copyfile(assetFilePath, crowdFilePath)
					symLinkException = True
		if symLinkException:
			self.report ({"WARNING"},"Crowds pulled as copies as blender is not ruuning in admin mode. Please re-open shot")
		else:
			self.report ({"INFO"},"Crowds pulled as Sym links. Please re-open shot")
			
		return {"FINISHED"}


class SaveShots(Operator):
	"""docstring for SaveShots"""
	bl_idname = "export.saveshots"
	bl_label = "Assetize Shot"
	bl_options = {'REGISTER'}

	sequences = CollectionProperty(type=Sequences)
	shots = CollectionProperty(type=Shots)
	selectedSequence = StringProperty(name="Sequence")
	selectedShot = StringProperty(name="Shot")
	versionComments = StringProperty(name="Version Comments")
	generateThumb = BoolProperty(name="Generate new thumbnail", default=False)

	def __init__(self):
		super(SaveShots, self).__init__()
		self._showData = None
		self._shotData = None

	def invoke(self, context, event):
		try:
			with open(preferences().project_path + "showConfig.json") as dataFile:
				self._showData = json.load(dataFile)
			self.sequences.clear()
			seqData = self._showData["sequences"]
			for eachSeqData in seqData:
				seq = self.sequences.add()
				seq.name = eachSeqData["sequenceOrder"] + "_" + eachSeqData["sequenceName"]

			self.selectedSequence = context.scene.asset_info.sequenceName
			self.selectedShot = context.scene.asset_info.assetName
		except Exception as e:
			self._showData = None
			self.report ({"ERROR"},"Please check project preferences")
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		if self._showData == None:
			layout.label("Project Preferences are not valid. Please fix it first!!")
			return
		layout.prop_search(self, "selectedSequence", self, "sequences")

		selectedSeqIndex = self.sequences.find(self.selectedSequence)
		if selectedSeqIndex == -1:
			return

		#Find seq
		seqData = None
		for eachSeqData in self._showData["sequences"]:
			if eachSeqData["sequenceOrder"] + "_" + eachSeqData["sequenceName"] == self.selectedSequence:
				seqData = eachSeqData

		self.shots.clear()

		for i in range(seqData["noOfShots"]):
			shot = self.shots.add()
			shot.name = seqData["sequenceName"] + "_" + ("%02d" % (i,))
		
		layout.prop_search(self, "selectedShot", self, "shots")

		selectedShotIndex = self.shots.find(self.selectedShot)
		if selectedShotIndex == -1:
			return

		try:
			with open(preferences().project_path + self.selectedSequence + "\\" + self.selectedShot + "\\info.json") as dataFile:
				self._shotData = json.load(dataFile)
			layout.label( "Current version of {0} is {1}".format(self.selectedShot ,self._shotData["version"]) )
			layout.prop(self, "versionComments")
			layout.prop(self, "generateThumb")
		except Exception as e:
			self._shotData = None
			layout.label( "Error Occured while reading Version Info") 


	def execute(self, context):
		if not self.selectedSequence or not self.selectedShot:
			self.report ({"ERROR"},"Sequence and Shot aren't properly selected")
			return {"CANCELLED"}

		if self._shotData == None:
			self.report ({"ERROR"},"There's something wrong while reading shotData")
			return {"CANCELLED"}

		self._shotData["version"] = self._shotData["version"] + 1
		self._shotData["versionComments"].append({
				"version" : self._shotData["version"],
				"comments" : self.versionComments
			})
		# context.scene.name = self.selectedSequence + "/" + self.selectedShot
		context.scene.asset_info.assetName = self._shotData["name"]
		context.scene.asset_info.assetType = self._shotData["type"]
		context.scene.asset_info.assetVersion = self._shotData["version"]
		context.scene.asset_info.sequenceName = self._shotData["seqName"]
		context.scene.asset_info.shotLength = self._shotData["length"]

		#Save file 
		shotPath = preferences().project_path + self.selectedSequence + "\\" + self.selectedShot 
		bpy.ops.wm.save_as_mainfile(
				filepath = blendPath(shotPath + "\\" + self.selectedShot),
				check_existing = False,
				relative_remap = False
			)

		#Update JSON
		with open(shotPath + "\\info.json", "w") as dataFile:
			json.dump(self._shotData, dataFile, indent=2)

		#Version 
		os.makedirs(preferences().datacenter_path + "{0}\\{1}".format(self.selectedSequence, self.selectedShot), exist_ok=True)
		bpy.ops.wm.save_as_mainfile(
				filepath = preferences().datacenter_path + "{0}\\{1}\\{1}_v{2}.blend".format(self.selectedSequence, self.selectedShot, self._shotData["version"]),
				check_existing = True,
				copy = True,
				relative_remap = False
			)

		#Thumbnail
		if not self.generateThumb:
			self.report ({"INFO"},"Shot versioning is completed")
			return {"FINISHED"}
			
		bpy.ops.view3d.viewnumpad(type='CAMERA')
		originalPercentage = bpy.context.scene.render.resolution_percentage
		bpy.context.scene.render.resolution_percentage = 25
		originalFormat = bpy.context.scene.render.image_settings.file_format
		bpy.context.scene.render.image_settings.file_format = 'PNG'
		bpy.context.scene.render.filepath = shotPath + "\\" + self.selectedShot + ".png"
		bpy.ops.render.opengl(write_still = True)
		bpy.context.scene.render.resolution_percentage = originalPercentage
		bpy.context.scene.render.image_settings.file_format = originalFormat

		self.report ({"INFO"},"Shot versioning is completed along with thumbnail")
		return {"FINISHED"}


	def check(self, context):
		return True


class SaveWIPAsset(Operator):
	"""docstring for SaveShots"""
	bl_idname = "export.savewipasset"
	bl_label = "Save WIP Asset"
	bl_options = {'REGISTER'}

	saveAs = StringProperty(name="Save As")

	def __init__(self):
		super(SaveWIPAsset, self).__init__()

	def invoke(self, context, event):
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		self.saveAs = "{0}_WIP_v{1}".format(context.scene.asset_info.assetName, context.scene.asset_info.assetVersion + 1)
		layout.prop(self, "saveAs")

	def execute(self, context):
		basePath = "---"
		if context.scene.asset_info.assetType == "SHOT":
			basePath = "{2}{0}\\{1}\\".format(context.scene.asset_info.sequenceName, context.scene.asset_info.assetName,preferences().project_path)
		elif context.scene.asset_info.assetType == "ASSET":
			basePath = "{1}assets\\{0}\\".format(context.scene.asset_info.assetName,preferences().project_path)

		if not os.path.exists(basePath):
			self.report ({"ERROR"},"Data stored is not good to save as WIP")
			return {"CANCELLED"}
			
		bpy.ops.wm.save_mainfile(
				filepath = blendPath(basePath + self.saveAs),
				check_existing = True,
				relative_remap = False
			)
		return {"FINISHED"}


class CreateRenderQueueJSON(Operator):
	"""docstring for SaveShots"""
	bl_idname = "export.renderjson"
	bl_label = "Generate Render Queue File"
	bl_options = {'REGISTER'}

	# To-Do valus defaults are hard coded for now
	remoteProjectRoot = StringProperty(name="Remote Project Path", default="/dmw/shows/llk/")

	renderPath = StringProperty(name="Render Path", default="\\renders/3draw")
	renderFrames = StringProperty(name="Render Frames", default="101")
	renderResolution = IntProperty(name="Render Resolution", default=50, min=1, max=100)

	saveName = StringProperty(name="Save As", default="")

	def invoke(self, context, event):
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		# self.renderPath = "{0}_WIP_v{1}".format(context.scene.asset_info.assetName, context.scene.asset_info.assetVersion + 1)
		assetInfo = context.scene.asset_info
		self.renderPath = "{0}{1}/{2}/renders/3draw".format(self.remoteProjectRoot, assetInfo.sequenceName, assetInfo.assetName )
		self.renderFrames = "{0} {1}".format(101, 100 + assetInfo.shotLength)
		self.saveName = "{0}rqItems\\{1}.json".format(preferences().project_path, assetInfo.assetName)
		layout.prop(self, "remoteProjectRoot")
		layout.prop(self, "renderPath")
		layout.prop(self, "renderFrames")
		layout.prop(self, "renderResolution")
		layout.prop(self, "saveName")

	def execute(self, context):
		assetInfo = context.scene.asset_info
		jsonData = {
			"src"  : "{0}{1}/{2}/{2}.blend".format(self.remoteProjectRoot, assetInfo.sequenceName, assetInfo.assetName),
			"output_path" : self.renderPath,
			"frames" : self.renderFrames,
			"resolution" : self.renderResolution
		}

		with open(self.saveName, "w") as dataFile:
			json.dump(jsonData, dataFile, indent=2)
		return {"FINISHED"}


class WorkOnFile(Operator):
	"""docstring for SaveShots"""
	bl_idname = "work.assetizer"
	bl_label = "Work on Asset/Shot"
	bl_options = {'REGISTER'}

	filepath = StringProperty(name="Open File", subtype='FILE_PATH')
	allFiles = CollectionProperty(type=Assets)
	selectedFile = StringProperty(name="File to Open")

	def __init__(self):
		super(WorkOnFile, self).__init__()
		self._dirPath = ""

	def invoke(self, context, event):
		self._dirPath = os.path.dirname(self.filepath)
		self.allFiles.clear()
		for files in sorted(os.listdir(self._dirPath)):
			fileName, ext = os.path.splitext(files)
			if ext == ".blend":
				newFile = self.allFiles.add()
				newFile.name = fileName + ".blend"
		self.selectedFile = os.path.basename(self.filepath)
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		layout.prop_search(self, "selectedFile", self, "allFiles")

	def execute(self, context):
		bpy.ops.wm.open_mainfile(filepath=self._dirPath + "\\" + self.selectedFile)
		return {"FINISHED"}

	def check(self, context):
		return True


class AssetizerPanel(Panel):
	bl_idname = "buttonpanel.assetizer"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	bl_label = "Assetizer"
	bl_description = ""
	bl_category = "DTR MediaWorks"

	def __init__(self):
		super(AssetizerPanel, self).__init__()

	def draw(self, context):
		layout = self.layout
		layout.operator('view3d.assetizer')
		layout.operator('export.assetizer')
		layout.operator('view3d.shotsmenu')
		layout.operator('export.saveshots')
		layout.operator('export.savewipasset')
		layout.operator('export.renderjson')


class AssetInfo(bpy.types.PropertyGroup):
	assetName = StringProperty(name="Name", default="---")
	assetType = StringProperty(name="Type", default="---")
	assetVersion = IntProperty(name="Version")
	sequenceName = StringProperty(name="Sequence", default="---")
	shotLength = IntProperty(name="Length")


class InfoPanel(Panel):
	bl_idname = "infopanel.assetizer"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	bl_label = "Asset Info"
	bl_description = ""
	bl_category = "DTR MediaWorks"

	def __init__(self):
		super(InfoPanel, self).__init__()

	def draw(self, context):
		layout = self.layout
		column = layout.column()
		row = column.row()
		row.label("Type : ")
		row.label(context.scene.asset_info.assetType)

		column = layout.column()
		row = column.row()
		row.label("Name : ")
		row.label(context.scene.asset_info.assetName)

		column = layout.column()
		row = column.row()
		row.label("Version : ")
		row.label(str(context.scene.asset_info.assetVersion))

		if context.scene.asset_info.assetType == "SHOT":
			column = layout.column()
			row = column.row()
			row.label("Length : ")

			column = row.column()
			row = column.column()
			row.label("{} frames".format(context.scene.asset_info.shotLength))
			row.label("({} - {})".format(101, 100 + context.scene.asset_info.shotLength))

			column = layout.column()
			column.operator(CrowdUpdater.bl_idname)


class SaveImageAsThumnail(Operator):
	"""docstring for SaveImageAsThumnail"""
	bl_idname = "assetizer.thumbnailer"
	bl_label = "Save Image as Thumbnail"
	bl_options = {'REGISTER'}

	def __init__(self):
		super(SaveImageAsThumnail, self).__init__()

	def execute(self, context):
		assetType = context.scene.asset_info.assetType
		savePath = ""
		if context.scene.asset_info.assetType == "SHOT":
			savePath = "{2}{0}\\{1}\\{1}.png".format(context.scene.asset_info.sequenceName, context.scene.asset_info.assetName,preferences().project_path)
		elif context.scene.asset_info.assetType == "ASSET":
			savePath = "{1}assets\\{0}\\{0}.png".format(context.scene.asset_info.assetName,preferences().project_path)

		bpy.ops.image.save_as(save_as_render=True, copy=True, filepath=savePath, relative_path=False)
		self.report ({"INFO"},"Thumbnail is changed successfully")
		return {"FINISHED"}
		
def saveImageAsThumbnail(self, context):
	self.layout.operator(SaveImageAsThumnail.bl_idname)

# store keymaps here to access after registration
addon_keymaps = []

def menu_draw(self, context):
	layout = self.layout
	layout.separator()
	layout.operator(AssetizerMenu.bl_idname, icon='MOD_SCREW')

def register():
	bpy.utils.register_class(Assets)
	bpy.utils.register_class(AssetGroups)
	bpy.utils.register_class(AssetizerMenu)
	bpy.utils.register_class(Assetize)
	bpy.utils.register_class(AssetLinker)
	bpy.utils.register_class(AssetInfo)
	bpy.utils.register_class(ShotsMenu)
	bpy.utils.register_class(Sequences)
	bpy.utils.register_class(Shots)
	bpy.utils.register_class(SaveShots)
	bpy.utils.register_class(SaveWIPAsset)
	bpy.utils.register_class(CreateRenderQueueJSON)
	bpy.utils.register_class(WorkOnFile)
	bpy.utils.register_class(CrowdsCreator)
	bpy.utils.register_class(CrowdUpdater)
	bpy.utils.register_class(SaveImageAsThumnail)
	# bpy.types.INFO_MT_mesh_add.append(menu_draw)
	bpy.types.IMAGE_MT_image.append(saveImageAsThumbnail)
	bpy.utils.register_class(AssetizerPreferences)
	bpy.utils.register_class(AssetizerPanel)
	bpy.utils.register_class(InfoPanel)
	bpy.types.Scene.asset_info = bpy.props.PointerProperty(type=AssetInfo)

	# handle the keymap
	wm = bpy.context.window_manager
	kc = wm.keyconfigs.addon
	if kc:
		km = kc.keymaps.new(name='3D View', space_type='VIEW_3D')
		kmi = km.keymap_items.new('view3d.assetizer', 'A', 'PRESS', ctrl=True, shift=True, alt=True)
		kmi = km.keymap_items.new('export.assetizer', 'E', 'PRESS', ctrl=True, shift=True, alt=True)
		kmi = km.keymap_items.new('view3d.shotsmenu', 'O', 'PRESS', ctrl=True, shift=True, alt=True)
		kmi = km.keymap_items.new('export.saveshots', 'R', 'PRESS', ctrl=True, shift=True, alt=True)
		kmi = km.keymap_items.new('export.savewipasset', 'S', 'PRESS', ctrl=True, shift=True, alt=True)
		addon_keymaps.append((km, kmi))

def unregister():
	bpy.utils.unregister_class(Assets)
	bpy.utils.unregister_class(AssetGroups)
	bpy.utils.unregister_class(AssetizerMenu)
	bpy.utils.unregister_class(Assetize)
	bpy.utils.unregister_class(AssetLinker)
	bpy.utils.unregister_class(AssetInfo)
	bpy.utils.unregister_class(ShotsMenu)
	bpy.utils.unregister_class(Sequences)
	bpy.utils.unregister_class(Shots)
	bpy.utils.unregister_class(SaveShots)
	bpy.utils.unregister_class(SaveWIPAsset)
	bpy.utils.unregister_class(CreateRenderQueueJSON)
	bpy.utils.unregister_class(WorkOnFile)
	bpy.utils.unregister_class(CrowdsCreator)
	bpy.utils.unregister_class(CrowdUpdater)
	bpy.utils.unregister_class(AssetizerPreferences)
	bpy.utils.unregister_class(AssetizerPanel)
	bpy.utils.unregister_class(InfoPanel)
	bpy.utils.unregister_class(SaveImageAsThumnail)
	del bpy.types.Scene.asset_info

	# handle the keymap
	for km, kmi in addon_keymaps:
		km.keymap_items.remove(kmi)
	addon_keymaps.clear()

if __name__ == "__main__":
	register()
	
# PYDEV_SOURCE_DIR = "C:\\devel\\apps\\eclipse-neon-pydev\\plugins\\org.python.pydev_5.8.0.201706061859\\pysrc"
# import sys
# if PYDEV_SOURCE_DIR not in sys.path:
#     sys.path.append(PYDEV_SOURCE_DIR)
# import pydevd
# pydevd.settrace()
